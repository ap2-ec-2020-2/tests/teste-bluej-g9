package Gabriela;

public class Kaori {
    public String nome;
    public String sobrenome;
    public String semestre;
    
    public String nomeCompleto (){
        return "Nome completo: " + this.nome + " " + this.sobrenome;
    }
    
    public String getSemestre (){
        return this.semestre;
    }
    
}