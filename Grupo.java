import Gabriela.Kaori;
import Victor.Victor;
import RaquelOliveira.Raquel;

public class Grupo
{   
   public static void main(String args[]){
        //Se instancie aqui
        Kaori gkaori = new Kaori();
        gkaori.nome = "Gabriela";
        gkaori.sobrenome = "Kaori";
        gkaori.semestre = "6º!";
            
        Victor victor = Victor.criaVictor();
    
        Raquel raquel = new Raquel();
        raquel.nome = "Raquel";
        raquel.sobrenome = "Oliveira";
        raquel.semestre = "segundo semestre";
        
        String nomeRaquel = raquel.nomeCompleto();
        String nomeDaKaori = gkaori.nomeCompleto();
        
        System.out.print("Membros:\n\n");
        
        victor.nomeCompleto();
        ListaTeste.listarMembros(nomeDaKaori);
        ListaTeste.listarMembros(nomeRaquel);
   }
}
