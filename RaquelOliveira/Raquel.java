package RaquelOliveira;

public class Raquel {
    public String nome, sobrenome, semestre;
    
    public String nomeCompleto() {
        return "Nome completo: " + this.nome + " " + this.sobrenome;
    }
    
    public void getSemestre() {
        System.out.println("Semestre: " + this.semestre);
    }
}