package Victor;



public class Victor {
    private final String nome;
    private final String sobrenome;
    protected String semestrePorExtenso;

    private Victor(String nome, String sobrenome, String semestrePorExtenso) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.semestrePorExtenso = semestrePorExtenso;
    }

    public static Victor criaVictor() {
        return new Victor("Victor", "Stevanovich", "Quinto-semestre");
    }

    public void nomeCompleto() {
        System.out.println("Meu nome é: " + this.nome + " " + this.sobrenome);
    }

    public String getSemestrePorExtenso() {
        return this.semestrePorExtenso;
    }
}
